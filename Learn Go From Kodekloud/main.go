//Every Go Executable Shoud be Single Go package - main and Main Function
package main

import "fmt"

//This is a Comment

/*
This is a
multi line comment
to track for testing
*/
func main() {
	fmt.Println("Hello World !!")
}
